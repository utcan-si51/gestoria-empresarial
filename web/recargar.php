  <article id="tabla">
    <div id="barra">
      <form id="buscador" action="BusquedaNueva.php">
        <div class="form-row align-items-center">
      <div class="col-auto my-1">
        <input type="text" name="Nombre" class="form-control" id="NombreServicio" placeholder="Nombre del Servicio" required>
      </div>
    <div class="col-auto my-1">
    <button type="submit" class="btn btn-primary mr-sm-2">Buscar Servicio</button>
    </div>
    </div>
      </form>
    </div>
       <table id="tablota" class="table  table-striped table-dark">
   <thead class="thead-dark">
     <tr>
       <th scope="col">#</th>
       <th scope="col">Nombre del Servicio</th>
       <th scope="col">Costo</th>
      <th scope="col">Tareas</th>
     </tr>
   </thead>
   <tbody id="tablita">
   </tbody>
 </table>
 </article>

  <article id="formulario1">
    <form id="formAgregar" class="form-control" action="ServicioNuevoQry.php" method="post">
      <h1>Nuevo Registro</h1>
      <div class="form-groupm form-control-sm">
        <label for="">Nombre</label>
        <input type="text" name="Nombre"class="form-control" id="Nombre" placeholder="Nombre" required>
      </div>
      <div class="form-group form-control-sm">
        <label for="">Costo</label>
        <input type="number" name="Costo"class="form-control" id="Costo" placeholder="Costo" required>
      </div>
      <button type="submit" class="btn btn-dark btn-sm">Agregar Servicio</button>
    </form>
  </article>
