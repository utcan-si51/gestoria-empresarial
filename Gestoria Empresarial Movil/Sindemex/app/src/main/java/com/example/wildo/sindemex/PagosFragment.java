package com.example.wildo.sindemex;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Spinner;


/**
 * A simple {@link Fragment} subclass.
 */
public class PagosFragment extends Fragment {


    public PagosFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view2 = inflater.inflate(R.layout.fragment_blank_fragment2, container, false);

        Spinner spinner = (Spinner) view2.findViewById(R.id.sp1);
        String[] letra = {"","Agregar Pago"};
        spinner.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, letra));

        Spinner spinner2 = (Spinner) view2.findViewById(R.id.sp2);
        String[] letra2 = {"","Agregar Pago"};
        spinner2.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, letra));
        return view2;
    }

}
