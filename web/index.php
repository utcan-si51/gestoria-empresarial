<?php
session_start();

if(!isset($_SESSION["IdActual"])){
  header("location: login.php");
}
require_once('Config/Conexion.php');
//instancia de la clase conexión
$conexion= new Conexion;
$id=$_SESSION["IdActual"];
$sql="select * from usuarios where usuario = '$id';";
$fila=$conexion->traerValores($sql);
?>
<html>
<head>
  <meta charset="utf-8"/>
  <script src="servicios.js"></script>
  <link rel="stylesheet" href="css/bootstrap.css"/>
  <link rel="stylesheet" href="css/main.css"/>
  <title>Gestoría Sindemex</title>
</head>
<body>
<?php
if($fila['Nivel']==1){
  echo '<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #DBEADC;"">
  <a class="navbar-brand" href="#">Sindemex</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">
      <li class="nav-item active">
        <a class="nav-link" href="index.php">Servicios <span class="sr-only">(current)</span></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="Clientes.php">Clientes</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="pagos.php">Pagos</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="login.php">Cerrar Sesión</a>
      </li>
    </ul>
  </div>
</nav>';
}else{
  echo '<nav class="navbar navbar-light " style="background-color: #e3f2fd;"">
  <a class="navbar-brand" href="#">Sindemex</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav">

      <li class="nav-item">
        <a class="nav-link" href="#"></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="#"></a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="login.php">Cerrar Sesión</a>
      </li>
    </ul>
  </div>
</nav>';
}
?>
<section id="todo">

  <article id="tabla">
    <div id="barra">
      <form id="buscador" action="BusquedaNueva.php">
        <div class="form-row align-items-center">
      <div class="col-auto my-1">
        <input type="text" name="Nombre" class="form-control" id="NombreServicio" placeholder="Nombre del Servicio">
      </div>
    <div class="col-auto my-1">
    <button type="submit" class="btn btn-primary mr-sm-2">Buscar</button>
    </div>
    </div>
      </form>
    </div>
       <table id="tablota" class="table  table-striped table-dark">
   <thead class="thead-dark">
     <tr>
       <th scope="col">#</th>
       <th scope="col">Nombre del Servicio</th>
       <th scope="col">Costo</th>
      <th scope="col">Tareas</th>
     </tr>
   </thead>
   <tbody id="tablita">
   </tbody>
 </table>
 </article>

  <article id="formulario1">
    <form id="formAgregar" class="form-control" action="ServicioNuevoQry.php" method="post">
      <h1>Nuevo Registro</h1>
      <div class="form-groupm form-control-sm">
        <label for="">Nombre</label>
        <input type="text" name="Nombre"class="form-control" id="Nombre" placeholder="Nombre" required>
      </div>
      <div class="form-group form-control-sm">
        <label for="">Costo</label>
        <input type="number" name="Costo"class="form-control" id="Costo" placeholder="Costo" required>
      </div>
      <button type="submit" class="btn btn-dark btn-sm">Agregar</button>
    </form>
  </article>
</section>
</body>
</html>
