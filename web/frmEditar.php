<?php
	require_once('Config/Conexion.php');
	//instancia de la clase conexión
	$mi_conexion= new Conexion;
	$IdAl =$_GET['id'];
	$sql="select * from clientes where Id=$IdAl";
	$fila=$mi_conexion->traerValores($sql);
	
?>

<br>
<h3>Ingrese los Nuevos Datos del Cliente</h3>
<form action="guardaClienteEditadoQry.php" method="POST" id="frmEditar">
	<input type="hidden" name="ID" value="<?php echo $fila['Id']; ?>" id="Id">
	<label>Nombre</label>
	<input type="text" id="nomb" name="Nombre" placeholder="Nombre" class="form-control" value="<?php echo $fila['Nombre']; ?>" required>
	<label>Dirección</label>
	<input type="text" id="direc" name="Direccion" placeholder="Dirección" class="form-control" value="<?php echo $fila['Direccion']; ?>" required>
	<label>Telefono</label>
	<input type="text" id="telef" name="Telefono" placeholder="Telefono" class="form-control" value="<?php echo $fila['Telefono']; ?>" required>
	<label>RFC</label>
	<input type="text" id="rfc" name="RFC" placeholder="RFC" class="form-control" value="<?php echo $fila['RFC']; ?>" required>
	<label>CURP</label>
	<input type="text" id="curp" name="CURP" placeholder="CURP" class="form-control" value="<?php echo $fila['CURP']; ?>" required>
	<label>ClabeInterbancaria</label>
	<input type="text" id="clbInter" name="ClabeInterbancaria" placeholder="ClabeInterbancaria" class="form-control" value="<?php echo $fila['ClabeInterbancaria']; ?>" required>
	<br>
	<button type="submit" class="btn btn-success">Editar</button>
	<button id="btnCancelarFrm" class="btn btn-danger" onclick="btnCancelar()">Cancelar</button>
</form>
<br>