<?php
	require_once('Config/Conexion.php');
	//instancia de la clase conexión
	$mi_conexion= new Conexion;

	session_start();
	if(!empty($_POST)){
		$usuario = mysqli_real_escape_string($mi_conexion->conexion,$_POST['usuario']);
		$password = mysqli_real_escape_string($mi_conexion->conexion,$_POST['contra']);
		// encriptamos la contraseña con el algoritmo sha1

		$sql = " select * FROM usuarios WHERE Usuario = '$usuario' AND Contrasenia = '$password'";
		// usamos el método CANTIDAD para saber el num de registros
		$registros =$mi_conexion->totalRegistros($sql);
		if($registros > 0) {
			// usamos el método traerValores para encontrar el usuario en especifico
			$fila=$mi_conexion->traerValores($sql);
			$_SESSION['IdActual'] = $fila['Usuario'];
			$_SESSION['Nombre'] = $fila['Nombre'];
			$_SESSION['Nivel'] = $fila['Nivel'];
			if(($_SESSION['Nivel']==1)){
				header("location: index.php");
		}else if(($_SESSION['Nivel']==2)){
			header("location: Clientes.php");
		}else if(($_SESSION['Nivel']==3)) {
			header("location: pagos.php");
		}
	}
}
?>
<!DOCTYPE html>
<html>
	<head>
		<meta charset="UTF-8">
		<link rel="stylesheet" href="css/login.css"/>
		<link rel="stylesheet" href="css/loginres.css"/>
		<script>
		$('document').ready(function(){
			$('#ap').click(function(){
				$('#login').hide();
				$('#registra').show();
			});

			$('#ap2').click(function(){
				$('#registra').hide();
				$('#login').show();
			});

			var cont=1;
					$('#icono_animacion').click(function(){
						if(cont==1){
							//$('nav').show('slow');
							$('nav').animate({
								left:'100%'
							});
							$('#icono_animacion').animate({
								left:'28%'
							});
							$('.form').animate({
								left:'60%'
							});
							cont=0;
						}else{
							cont=1;
							$('nav').animate({
								left:'0'
							});
							$('#icono_animacion').animate({
								left:'0'
							});
							$('#icono_animacion').animate({
								left:'1%'
							});
							$('.form').animate({
								left:'0%'
							});
						}
					});
		});
		</script>
	</head>
	<body>

		<header>
			<a id="icono_animacion" href="#"><img src="images/header/icono.png" id="icono"/></a>
			<nav>
				<h2 id="menu">Menú</h2>
				<ul>
					<li><a href="index.php">Inicio</a></li>
					<li><a href="Cancun.php">Cancún</a></li>
					<li><a href="RivieraM.php">Riviera Maya</a></li>
					<li><a href="#">Galeria</a>
						<ul>
							<li><a href="Galeria_C.php">Cancún</a></li>
							<li><a href="Galeria_M.php">Riviera Maya</a></li>
						</ul>
					</li>

					<li><a href="login.php">Login</a></li>
				</ul>
			</nav>
		</header>
		<div class="login">
		  <div class="form">
			<form class="registro-form" id="registra" method="post" action="admin/usuario_nuevoQry.php">
				<input type="text" name="nombre" placeholder="Nombre" required />
				<input type="text" name="cuenta" placeholder="Correo Electronico" required />
				<input type="password" name="contraseña" placeholder="Contraseña" required />
				<button>Crear</button>
				<p class="message">Ya estas Registrado?<a href="#" rel="nofollow" id="ap2">Inicia Sesion</a></p>
			</form>
			<form class="login-form" action="<?php $_SERVER['PHP_SELF']; ?>" method="POST" id="login">
				<input type="text" name="usuario" placeholder="Usuario" required />
				<input type="password" name="contra" placeholder="Contraseña" required />
				<button type="submit">Login</button>

			</form>
		  </div>
		</div>
	</body>
</html>
