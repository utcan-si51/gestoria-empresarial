package com.example.wildo.sindemex;


import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.app.Fragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;


/**
 * A simple {@link Fragment} subclass.
 */
public class ClientesFragment extends Fragment {


    public ClientesFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View vw = inflater.inflate(R.layout.fragment_clientes, container, false);
        final Spinner spinner = (Spinner) vw.findViewById(R.id.spin);
        String[] letra = {"","Editar","Eliminar", "Agregar Servicio"};
        spinner.setAdapter(new ArrayAdapter<String>(getContext(), android.R.layout.simple_spinner_item, letra));

        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {

                switch (position) {
                    case 0:

                        break;
                    case 1:
                        Intent frmEditar = new Intent(getContext().getApplicationContext(), ClientesEditarForm.class);
                        spinner.setSelection(0);
                        startActivity(frmEditar);
                        break;
                    case 2:
                        spinner.setSelection(0);
                        new AlertDialog.Builder(getContext())
                                .setTitle("Eliminar Cliente")
                                .setMessage("Cliente Eliminado Correctamente").show();
                        break;
                    case 3:
                        Intent tblServ = new Intent(getContext().getApplicationContext(), ClientesAgregarServicio.class);
                        spinner.setSelection(0);
                        startActivity(tblServ);
                        break;
                }
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });

        FloatingActionButton btnNuevo = (FloatingActionButton) vw.findViewById(R.id.nuevo);

        btnNuevo.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view){
                Intent frmNuevo = new Intent(getContext().getApplicationContext(), ClienteNuevo.class);
                startActivity(frmNuevo);
            }
        });

        return vw;
    }

}
