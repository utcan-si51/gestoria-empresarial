addEventListener('load', inicializaEvento, false);

function inicializaEvento(e){
	var url = "tabla.php";
	enviar(url);
}

var conexion;

function enviar(url){
	conexion = new XMLHttpRequest();
	conexion.onreadystatechange=procesaEvento;
	conexion.open('GET', url, true);
	conexion.send();
}

function procesaEvento(){
	var seccionT = document.getElementById('tabla');
	if (conexion.readyState==4) {
		seccionT.innerHTML = conexion.responseText;
		inicializaEvento2(); //Formulario Cliente
		inicializaEvento4(); //Formulario Editar
		inicializaEvento6(); //Eliminar Cliente
		incializaEvento7(); //Buscador
		inicializaEvento8(); //Tabla Servicios
	}else{
		seccionT.innerHTML = 'Cargando...';
	}
}

////////////////////////////////////////Formulario/////////////////////////

function inicializaEvento2(){
	var btn = document.getElementById('btnNuevo');
	btn.addEventListener('click', presionaBoton2, false);
}

function presionaBoton2(e){
	e.preventDefault();
	var url2 = document.getElementById('btnNuevo').value;
	//alert (url);
	enviar2(url2);
}

var conexion2;

function enviar2(url2){
	conexion2 = new XMLHttpRequest();
	conexion2.onreadystatechange=procesaEvento2;
	conexion2.open('GET', url2, true);
	conexion2.send();
}

function procesaEvento2(){
	var seccion = document.getElementById('formulario');
	if (conexion2.readyState==4) {
		seccion.innerHTML = conexion2.responseText;
		inicializaEvento3();
	}else{
		seccion.innerHTML = 'Cargando...';
	}
}

/////////////////////////////////////Guardar Cliente Nuevo///////////////////

function inicializaEvento3(){
	var frm = document.getElementById('frmGuardar');
	frm.addEventListener('submit', presionaBoton3, false);
}

function presionaBoton3(e){
	e.preventDefault();
	var url3 = 'guardaClienteQry.php';
	enviar3(url3);
}

var conexion3;

function enviar3(url3){
	conexion3 = new XMLHttpRequest();
	conexion3.onreadystatechange=procesaEvento3;
	conexion3.open("POST", url3, true);
	conexion3.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	conexion3.send(obtenerDatos());
}

function obtenerDatos(){
	var data = "";
	var dato1 = document.getElementById('nomb').value;
	var dato2 = document.getElementById('direc').value;
	var dato3 = document.getElementById('telef').value;
	var dato4 = document.getElementById('rfc').value.toUpperCase();
	var dato5 = document.getElementById('curp').value.toUpperCase();
	var dato6 = document.getElementById('clbInter').value;
	data = "var1="+encodeURIComponent(dato1)+"&var2="+encodeURIComponent(dato2)+"&var3="+encodeURIComponent(dato3)+"&var4="+encodeURIComponent(dato4)+"&var5="+encodeURIComponent(dato5)+"&var6="+encodeURIComponent(dato6);
	//alert(dato1);
	return data;
}

function procesaEvento3(){
	var detalles = document.getElementById("tabla");
	var frm = document.getElementById("formulario");
	//alert (conexion3.readyState);
	if(conexion3.readyState == 4){
		detalles.innerHTML = conexion3.responseText;
		frm.innerHTML = "";
		inicializaEvento2();
		inicializaEvento4();
		inicializaEvento6();
		inicializaEvento8();
	}else{
		detalles.innerHTML = "Cargando...";
	}
}

////////////////////////////////////Formulario Editar/////////////////////////////////

function inicializaEvento4(){
	var index = document.getElementById('tablaPrincipal').rows.length;
	for(var f = 0; f <= index; f++){
		//alert ("Fila No. " + f);
			var idEnlace = document.getElementById('btnEditar'+f);
		if(idEnlace){
			//alert ('Hola ' + idEnlace);
			idEnlace.addEventListener('click',presionaEnlace4,false)
			//alert(idEnlace.value)
		}
	}
}

function presionaEnlace4(e){
	e.preventDefault();
	var url4 = e.target.getAttribute('value');
	//alert(url4);
	enviar4(url4);
}

var conexion4;

function enviar4(url4){
	conexion4 = new XMLHttpRequest();
	conexion4.onreadystatechange=procesaEvento4;
	conexion4.open('GET', url4, true);
	conexion4.send();
}

function procesaEvento4(){
	var frmEditar = document.getElementById('formulario');
	if(conexion4.readyState==4){
		frmEditar.innerHTML = conexion4.responseText;
		inicializaEvento2();
		inicializaEvento5();
		inicializaEvento4();
		inicializaEvento6();
		inicializaEvento8();
	}else{
		frmEditar.innerHTML = 'Cargando...'
	}
}

//////////////////////Boton Cancelar Formulario Editar///////////////////////////

function btnCancelar(){
	var frmEditar = document.getElementById('formulario');
	frmEditar.innerHTML = "";
}

////////////////////Guardar CLiente Editado///////////////////////////////////

function inicializaEvento5(){
	var frmEditar = document.getElementById('frmEditar');
	frmEditar.addEventListener('submit', presionaEnlace5, false);
}

function presionaEnlace5(e){
	e.preventDefault();
	var url5 = "guardaClienteEditadoQry.php";
	var mensaje = confirm("¿Esta Seguro de Editar este Cliente?");
	if(mensaje){
		enviar5(url5);
	}
}

var conexion5;

function enviar5(url5){
	conexion5 = new XMLHttpRequest;
	conexion5.onreadystatechange=procesaEvento5;
	conexion5.open("POST", url5, true);
	conexion5.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	conexion5.send(obtenerDatos2());
}

function obtenerDatos2(){
	var data="";
	var dato0 = document.getElementById('Id').value;
	var dato1 = document.getElementById('nomb').value;
	var dato2 = document.getElementById('direc').value;
	var dato3 = document.getElementById('telef').value;
	var dato4 = document.getElementById('rfc').value.toUpperCase();
	var dato5 = document.getElementById('curp').value.toUpperCase();
	var dato6 = document.getElementById('clbInter').value;
	data = "var0="+encodeURIComponent(dato0)+"&var1="+encodeURIComponent(dato1)+"&var2="+encodeURIComponent(dato2)+"&var3="+encodeURIComponent(dato3)+"&var4="+encodeURIComponent(dato4)+"&var5="+encodeURIComponent(dato5)+"&var6="+encodeURIComponent(dato6);
	return data;
}

function procesaEvento5(){
	var tabla = document.getElementById('tabla');
	var frm = document.getElementById('formulario');
	//alert(conexion.readyState);
	if(conexion.readyState==4){
		tabla.innerHTML = conexion5.responseText;
		//alert("Cliente Editado Correctamente");
		inicializaEvento();
		//inicializaEvento4();
		//inicializaEvento6();
		//inicializaEvento8();
	}else{
		tabla.innerHTML = 'Cargando...'
	}
}

/////////////////////////Eliminar Cliente////////////////////

function inicializaEvento6(){
	var index = document.getElementById('tablaPrincipal').rows.length;
	for(var f = 0; f <= index; f++){
		//alert ("Fila No. " + f);
			var idEnlace = document.getElementById('btnEliminar'+f);
		if(idEnlace){
			//alert ('Hola ' + idEnlace);
			idEnlace.addEventListener('click',presionaEnlace6,false)
			//alert(idEnlace.value)
		}
	}
}

function presionaEnlace6(e){
	e.preventDefault();
	var url6 = e.target.getAttribute('value');
	var mensaje = confirm("¿Esta seguro de Eliminar al Cliente?");
	if(mensaje){
		enviar6(url6);
	}
}

var conexion6;

function enviar6(url6){
	conexion6 = new XMLHttpRequest();
	conexion6.onreadystatechange=procesaEvento6;
	conexion6.open('POST', url6, true);
	conexion6.send();
}

function procesaEvento6(){
	var tabla = document.getElementById('tabla');
	var frm = document.getElementById('formulario');
	if(conexion6.readyState==4){
		tabla.innerHTML = conexion6.responseText;
		inicializaEvento2();
		inicializaEvento4();
		inicializaEvento6();
		inicializaEvento8();
		frm.innerHTML = "";
	}else{
		tabla.innerHTML = 'Cargando...'
	}
}

//////////////////////////Buscador/////////////////////////////////

function incializaEvento7(){
	var buscador = document.getElementById('buscador');
	buscador.addEventListener('submit', presionaEnlace7, false);
}

function presionaEnlace7(e){
	e.preventDefault();
	var url7 = 'BuscarQry.php';
	enviar7(url7);
}

var conexion7;

function enviar7(url7){
	conexion7 = new XMLHttpRequest();
	conexion7.onreadystatechange=procesaEvento7;
	conexion7.open('POST', url7, true);
	conexion7.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	conexion7.send(obtenerDato());
}

function obtenerDato(){
	var data ="";
	var dato1 = document.getElementById('BNomb').value;
	//alert(dato1);
	data = "var1="+encodeURIComponent(dato1);
	return data;
}

function procesaEvento7(){
	var tabla = document.getElementById('tabla');
		//alert(conexion7.readyState);
	if(conexion7.readyState==4){
		tabla.innerHTML = conexion7.responseText;
		inicializaEvento2();
		inicializaEvento4();
		inicializaEvento6();
		incializaEvento7();
		inicializaEvento8();
	}else{
		tabla.innerHTML = 'Cargando...'
	}
}

//////////////////////Tabla Servicios////////////////////////////////

function inicializaEvento8(){
	var index = document.getElementById('tablaPrincipal').rows.length;
	for(var f = 0; f <= index; f++){
		//alert ("Fila No. " + f);
			var idEnlace = document.getElementById('btnServicios'+f);
		if(idEnlace){
			//alert ('Hola ' + idEnlace);
			idEnlace.addEventListener('click', presionaEnlace8, false);
		}
	}
}

function presionaEnlace8(e){
	e.preventDefault();
	var url8 = e.target.getAttribute('value');
	//alert(url8);
	enviar8(url8);
}

var conexion8;

function enviar8(url8){
	conexion8 = new XMLHttpRequest();
	conexion8.onreadystatechange=procesaEvento8;
	//alert(url8);
	conexion8.open('GET', url8, true);
	conexion8.send();
}

function procesaEvento8(){
	var servi = document.getElementById('tabla');
	var frm = document.getElementById('formulario');
	//alert(conexion.readyState);
	if(conexion8.readyState==4){
		servi.innerHTML=conexion8.responseText;
		frm.innerHTML = "";
		inicializaEvento9();
	}else{
		servi.innerHTML = 'Cargando...';
	}
}

//////////////////////////////////////Agregar Servicio al Cliente///////////////////////////////////

function inicializaEvento9(){
	var index = document.getElementById('tablaPrincipal').rows.length;
	for(var f = 0; f <= index; f++){
		//alert ("Fila No. " + f);
			var idEnlace = document.getElementById('btnAgServ'+f);
		if(idEnlace){
			//alert ('Hola ' + idEnlace);
			idEnlace.addEventListener('click', presionaEnlace9, false);
		}
	}
}

function presionaEnlace9(e){
	e.preventDefault();
	var url9 = e.target.getAttribute('value');
	var mensaje = confirm("¿Esta Seguro de Agregar Este Servicio?");
	if(mensaje){
		enviar9(url9);
	}
}

var conexion9;

function enviar9(url9){
	conexion9 = new XMLHttpRequest();
	conexion9.onreadystatechange=procesaEvento9;
	conexion9.open('POST', url9, true);
	conexion9.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
	conexion9.send(obtenerDatos3());
}

function obtenerDatos3(){
	var data="";
	var dato1 = document.getElementById('IdCli').value;
	//alert(dato1);
	data = "var1="+encodeURIComponent(dato1);
	return data;
}

function procesaEvento9(){
	if(conexion9.readyState==4){
		alert("Agregado Correctamente");
	}
}
