<?php
session_start();

if(!isset($_SESSION["IdActual"])){
  header("location: login.php");
}
require_once('Config/Conexion.php');
//instancia de la clase conexión
$conexion= new Conexion;
$id=$_SESSION["IdActual"];
$sql="select * from usuarios where usuario = '$id';";
$fila=$conexion->traerValores($sql);
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<title>Clientes</title>
	<link rel="stylesheet" type="text/css" href="css/main.css">
	<link rel="stylesheet" href="css/bootstrap.css">
	<script src="js/clientes.js"></script>
</head>
<body id="todo">
	<?php
	if($fila['Nivel']==1){
	  echo '<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #DBEADC;"">
	  <a class="navbar-brand" href="#">Sindemex</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarNav">
	    <ul class="navbar-nav">
	      <li class="nav-item active">
	        <a class="nav-link" href="index.php">Servicios <span class="sr-only">(current)</span></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="Clientes.php">Clientes</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="pagos.php">Pagos</a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="login.php">Cerrar Sesión</a>
	      </li>
	    </ul>
	  </div>
	</nav>';
	}else{
	  echo '<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #DBEADC;"">
	  <a class="navbar-brand" href="#">Sindemex</a>
	  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
	    <span class="navbar-toggler-icon"></span>
	  </button>
	  <div class="collapse navbar-collapse" id="navbarNav">
	    <ul class="navbar-nav">
	      <li class="nav-item active">

	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#"></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="#"></a>
	      </li>
	      <li class="nav-item">
	        <a class="nav-link" href="login.php">Cerrar Sesión</a>
	      </li>
	    </ul>
	  </div>
	</nav>';
	}
	 ?>
	<main>
		<header id="buscadores">
			<form action="nombreClienteQry.php" method="POST" id="buscador" class="input-group mb-3">
				<input type="text" name="txtNombre" placeholder="Escriba un nombre" class="form-control" id="BNomb">
				<div class="input-group-append">
					<button type="submit" class="btn btn-outline-secondary" id="btnBuscar">Buscar</button>
				</div>
			</form>
			<!--<form action="claveClienteQry.php" method="POST" id="buscador2" class="input-group mb-3">
				<input type="text" name="txtNombre" placeholder="Escriba una clave" class="form-control">
				<div class="input-group-append">
					<button class="btn btn-outline-secondary">Buscar</button>
				</div>
			</form>-->
		</header>
		<article id="tabla-frm">
			<article id="tabla">
			</article>
			<article id="formulario">

			</article>
			<div id="ready">

			</div>
		</article>
	</main>
</body>
</html>
