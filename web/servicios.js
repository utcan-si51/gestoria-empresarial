addEventListener('load',inicializarEvento,false);
var conexion1;
function inicializarEvento(){
  conexion1 = new XMLHttpRequest();
  conexion1.onreadystatechange = procesar;
  conexion1.open("GET", 'CargarTablas.php', true);
  conexion1.send();
}

function procesar(){
		var detalles = document.getElementById("tablita");
		if(conexion1.readyState ==4){
			detalles.innerHTML = conexion1.responseText;
      cargarBotonesEditar();
      botonAgregar();
      CargarbotonesEliminar();
      botonBuscar();
		}
		else{
			detalles.innerHTML = 'Cargando...';
		}
}
//cargar eventos al buscador
function botonBuscar(){
  var ob = document.getElementById('buscador');
  ob.addEventListener('submit',cargarBusqueda,false);
}


function cargarBusqueda(e){
  e.preventDefault();
  enviarBusqueda();
}

var conexionEditar;

function enviarBusqueda(){
  conexionEditar = new XMLHttpRequest();
  conexionEditar.open("POST", 'BusquedaNueva.php', true);
  conexionEditar.setRequestHeader("content-Type","application/x-www-form-urlencoded");
  conexionEditar.send(obtenerDatosBusqueda());
  conexionEditar.onreadystatechange = procesarBusqueda;
}

function obtenerDatosBusqueda(){
  var data="";
  var Nombre=document.getElementById('NombreServicio').value;
  data="var0="+encodeURIComponent(Nombre);
  return data;
}

function procesarBusqueda() {
  var detalles = document.getElementById("tablita");
  if(conexionEditar.readyState ==4){
    detalles.innerHTML = conexionEditar.responseText;
    cargarBotonesEditar();
    botonAgregar();
    CargarbotonesEliminar();
    botonBuscar();
  }
  else{
    detalles.innerHTML = 'Cargando...';
  }
}

//cargaLoseventos al boton Agregar
function botonAgregar(){
  var ob = document.getElementById('formAgregar');
  ob.addEventListener('submit',cargarDefault,false);
}


function cargarDefault(e){
  e.preventDefault();
  enviarAgregar();
}

var conexionAgregar;

 function enviarAgregar(){
   conexionAgregar = new XMLHttpRequest();
   conexionAgregar.open("POST", 'ServicioNuevoQry.php', true);
   conexionAgregar.setRequestHeader("content-Type","application/x-www-form-urlencoded");
   conexionAgregar.send(obtenerDatosAgregar());
   conexionAgregar.onreadystatechange = procesarAgregar;
 }

 function obtenerDatosAgregar(){
   var data="";
   var Nombre=document.getElementById('Nombre').value;
   var Costo=document.getElementById('Costo').value;
   data="var0="+encodeURIComponent(Nombre)+"&var1="+encodeURIComponent(Costo);
   return data;
 }

 function procesarAgregar(){
   var detalles = document.getElementById("todo");
   if(conexionAgregar.readyState ==4){
     detalles.innerHTML = conexionAgregar.responseText;
     inicializarEvento();
   }
   else{
     detalles.innerHTML = 'Cargando...';
   }
 }
//termina agregar
//Para cargar los eventos a los botones de editar
function cargarBotonesEditar(){
  var yea=document.getElementById("tablota").rows.length-1;
  for (var f = 0 ; f < yea; f++) {
      var ob = document.getElementById('ID'+f);
      ob.addEventListener('click',cargarForm,false);
  }
}

  function cargarForm(e){
    e.preventDefault();
  	var url= e.target.getAttribute('href');
  	cargarProgra(url)
  }


      var conex1;

  function cargarProgra(url){
  	conex1 = new XMLHttpRequest();
  	conex1.open("GET",url, true);
  	conex1.send();
  	conex1.onreadystatechange = procesarEventos;
  }

  function procesarEventos(){
  	var detalles = document.getElementById("formulario1");
  	if(conex1.readyState == 4){
  		detalles.innerHTML = conex1.responseText;
  		inicializarEvento2();
      inicializarEvento3();
  	}else{
  		detalles.innerHTML = 'Cargando...';
  	}
  	}
//Eventos del boton de submit

    function inicializarEvento2(){
    	var ob = document.getElementById('formEditar');
    	ob.addEventListener('submit',presionarEnlace2,false);
    }

    function presionarEnlace2(e){
    	e.preventDefault();
    	enviar2();
    }

    var conexion2;

    function enviar2(){
    		conexion2 = new XMLHttpRequest();
    		conexion2.open("POST", 'ServicioEditarQry.php', true);
    		conexion2.setRequestHeader("content-Type","application/x-www-form-urlencoded");
    		conexion2.send(obtenerDatos2());
    		conexion2.onreadystatechange = procesar2;
    }


    function procesar2(){
    		var detalles = document.getElementById("todo");
    		if(conexion2.readyState ==4){
    			detalles.innerHTML = conexion2.responseText;
           inicializarEvento();
    		}
    		else{
    			detalles.innerHTML = 'Cargando...';
    		}
    }


    function obtenerDatos2(){
    	var data="";
    	var ID=document.getElementById('ID').value;
    	var Nombre=document.getElementById('Nombre').value;
    	var Costo=document.getElementById('Costo').value;
    	data="var0="+encodeURIComponent(ID)+"&var1="+encodeURIComponent(Nombre)+"&var2="+encodeURIComponent(Costo);
    	return data;
    }
    //Eventos boton de atrás
    function inicializarEvento3(){
      var ob = document.getElementById('atras');
    	ob.addEventListener('click',presionarEnlace3,false);
    }

    function presionarEnlace3(e){
    	e.preventDefault();
    	enviar3();
    }

    var conexion3;

    function enviar3(){
    		conexion3 = new XMLHttpRequest();
    		conexion3.open("GET", 'recargar.php', true);
    		conexion3.send();
    		conexion3.onreadystatechange = procesar3;
    }

    function procesar3(){
      var detalles = document.getElementById("todo");
      if(conexion3.readyState ==4){
        detalles.innerHTML = conexion3.responseText;
        inicializarEvento();
      }
      else{
        detalles.innerHTML = 'Cargando...';
      }
    }

//Termina Editar empieza eliminar
  function CargarbotonesEliminar(){
  		var yea=document.getElementById("tablota").rows.length-1;
  		for (var f = 0 ; f < yea; f++) {
  				var ob = document.getElementById('IDE'+f);
  				ob.addEventListener('click',cargarEliminar,false);
  		}
  }

function cargarEliminar(e){
  e.preventDefault();
  var url= e.target.getAttribute('href');
  enviarEliminar(url);
}

var conex2;

function enviarEliminar(url){
  conex2 = new XMLHttpRequest();
  conex2.open("GET",url, true);
  conex2.send();
  conex2.onreadystatechange = procesarEliminar;
}

function procesarEliminar(){
    var detalles = document.getElementById("todo");
    if(conex2.readyState ==4){
      detalles.innerHTML = conex2.responseText;
       inicializarEvento();
    }
    else{
      detalles.innerHTML = 'Cargando...';
    }
}
