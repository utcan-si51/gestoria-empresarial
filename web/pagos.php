<?php
session_start();

if(!isset($_SESSION["IdActual"])){
  header("location: login.php");
}
require_once('Config/Conexion.php');
//instancia de la clase conexión
$conexion= new Conexion;
$id=$_SESSION["IdActual"];
$sql="select * from usuarios where usuario = '$id';";
$fila=$conexion->traerValores($sql);
?>
<html>
<head>
<meta charset="utf-8">
<title>Formulario de Pagos</title>
<link href="main.css" rel="stylesheet" type="text/css">
<link href="css/bootstrap.css" rel="stylesheet" type="text/css">
<script src="script.js"></script>
</head>
<body>
  <?php
  if($fila['Nivel']==1){
    echo '<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #DBEADC;"">
    <a class="navbar-brand" href="#">Sindemex</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item active">
          <a class="nav-link" href="index.php">Servicios <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="Clientes.php">Clientes</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="pagos.php">Pagos</a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="login.php">Cerrar Sesión</a>
        </li>
      </ul>
    </div>
  </nav>';
  }else{
    echo '<nav class="navbar navbar-expand-lg navbar-light" style="background-color: #DBEADC;"">
    <a class="navbar-brand" href="#">Sindemex</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
    <div class="collapse navbar-collapse" id="navbarNav">
      <ul class="navbar-nav">
        <li class="nav-item active">
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="#"></a>
        </li>
        <li class="nav-item">
          <a class="nav-link" href="login.php">Cerrar Sesión</a>
        </li>
      </ul>
    </div>
  </nav>';
  }
   ?>
<main id="all">
      <article id="formulario">
      <form id="formulario1" class="form-control" method="post" action="busqueda.php" >
    <div class="form-groupm form-control-sm">
          <label for="">Busqueda</label>
          <input type="text" name="search"class="form-control" id="search" placeholder="Busqueda por Nombre">
        </div>
        <button type="submit" class="btn btn-success btn-sm">Buscar</button>
      </form>
</div>
<article id="table">


</article>
 <article id="formulario4">

</article>
</article>

  </main>
</body>
</html>
